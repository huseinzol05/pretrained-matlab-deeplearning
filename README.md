# MATLAB load pretrained models

## Description

Tested on 2018a, but supported 2017a above, based on documentation.

## Models

- [x] alexnet
- [x] vgg16
- [x] vgg19
- [ ] inceptionv3

## Results

alexnet 

![alt text](alexnet/result.png)

vgg16 

![alt text](vgg16/result.png)

vgg19 

![alt text](vgg19/result.png)

